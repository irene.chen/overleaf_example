#!/bin/bash

FILE_PATH=/projects/Overleaf/ExampleToGitlab/overleaf_download_src.py
READ_LINK_PARAMETER=fspqmdvzkzpg
URL_PARAMETER=5dfb4b0bc3cb010001da226bm
OUTPUT_FOLDER=/projects/Overleaf/ExampleToGitlab
GIT_USER_EMAIL=irene.chen@skywatch.com.tw
GIT_USER_NAME=irene.chen

/opt/conda/bin/python $FILE_PATH --read_link_param $READ_LINK_PARAMETER \
                                --url_param $URL_PARAMETER \
                                --output_dir $OUTPUT_FOLDER
cd $OUTPUT_FOLDER
#/opt/conda/bin/python /projects/Overleaf/ExampleToGitlab/overleaf_download_src.py --read_link_param fspqmdvzkzpg \
#                                --url_param 5dfb4b0bc3cb010001da226bm \
#                                --output_dir $OUTPUT_FOLDER
#cd $OUTPUT_FOLDER
git config --global user.email $GIT_USER_EMAIL
git config --global user.name $GIT_USER_NAME

git add .
git commit -m 'update overleaf'
git pull origin master
git push origin master
