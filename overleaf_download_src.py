import os   
import requests
from argparse import ArgumentParser
import os
import zipfile

#repo = Repo('.')
parser = ArgumentParser()
#parser.add_argument("--read_link_param", type=str, default="fspqmdvzkzpg",
#        help="number in share link for viewing")
#parser.add_argument("--url_param", type=str, default="5dfb4b0bc3cb010001da226bm",
#        help="number in overleaf url")
#parser.add_argument("--output_dir", type=str, default="/projects/Overleaf/ExampleToGitlab",
#        help="output directory for your overleaf files")
parser.add_argument("--read_link_param", type=str,
        help="number in share link for viewing")
parser.add_argument("--url_param", type=str,
        help="number in overleaf url")
parser.add_argument("--output_dir", type=str,
        help="output directory for your overleaf files")

args = parser.parse_args()
print(args.read_link_param)
print(args.url_param)
print(args.output_dir)
try:
    # access url to get cookies 
    resp = requests.get("https://www.overleaf.com/read/{}".format(args.read_link_param))
    cookies = resp.cookies

    # download file
    res2 = requests.get("https://www.overleaf.com/project/{}/download/zip".format(args.url_param), cookies=cookies)
    with open('{}/src.zip'.format(args.output_dir), 'wb') as f:
        print('{}/src.zip'.format(args.output_dir))
        f.write(res2.content)

    with zipfile.ZipFile("{}/src.zip".format(args.output_dir), 'r') as zip_ref:
        zip_ref.extractall(args.output_dir)
except:
    pass
