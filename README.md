## Change parameters in overleaf2gitlab.sh:

* FILE_PATH: the absolute path of overlef_download_src.py

* $READ_LINK_PARAMETER: the generated string in the shared link (view) for your project

    * Ex. Share link: https://www.overleaf.com/read/wnczrxnrdsbt, then $READ_LINK_PARAMETER is **wnczrxnrdsbt**

* $URL_PARAMETER: the generated string for your project url.

    * Ex. Url: https://www.overleaf.com/project/5dfb4b0bc3cb010001da226bm then $URL_PARAMETER is **5dfb4b0bc3cb010001da226bm**

* $OUTPUT_FOLDER is the destination of your overleaf files. Remember to link the folder to your git project first.

* GIT_USER_EMAIL: your git user email

* GIT_USER_NAME: your git user name

## Run:
Add the task in crontab 
Example:
```
crontab -e
Inside the file, add the command with your preference: */1 * * * * sh /projects/Overleaf/ExampleToGitlab/overleaf2gitlab.sh > /projects/Overleaf/ExampleToGitlab/error.log 2>&1
/etc/init.d/cron start
```
